jQuery(document).ready(function ($) {
    /* BX slider 1*/
    if ($('#banner_slider').length) {
        $('#banner_slider').bxSlider({
            auto: true,
            minSlides: 1,
            maxSlides: 1,
            slideMargin: 18,
            speed: 500
        });
    }

    if ($('#news_slider').length) {
        $('#news_slider').bxSlider({
            minSlides: 1,
            maxSlides: 1,
            slideMargin: 18,
            speed: 500,
        });
    }

    if ($('.video_slider').length) {
        $('.video_slider').bxSlider({
            minSlides: 1,
            maxSlides: 1,
            slideMargin: 25,
            speed: 500,
        });
    }

    if ($('#blog_slider').length) {
        $('#blog_slider').bxSlider({
            minSlides: 1,
            maxSlides: 1
        });
    }

    if ($('#shop_slider').length) {
        $('#shop_slider').bxSlider({
            slideWidth: 140,
            minSlides: 1,
            maxSlides: 3,
            slideMargin: 28
        });
    }

    if ($('#office_slider').length) {
        $('#office_slider').bxSlider({
            slideWidth: 270,
            minSlides: 1,
            maxSlides: 4,
            slideMargin: 28
        });
    }

    if ($('#slider_products').length) {
        $('#slider_products').bxSlider({
            slideWidth: 270,
            minSlides: 1,
            maxSlides: 1,
            slideMargin: 10
        });
    }

    /* bootstrap Add class to accordion **/
    var sidebar = $('.accordion-heading'); /* cache sidebar to a variable for performance */
    sidebar.delegate('.accordion-toggle', 'click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).addClass('inactive');
            $("#icon_toggle i", this).removeClass('icon-minus').addClass('icon-plus');
        } else {
            sidebar.find('.active').addClass('inactive');
            sidebar.find('.active').removeClass('active');
            $(this).removeClass('inactive');
            $(this).addClass('active');
            $("#icon_toggle i", this).removeClass('icon-plus').addClass('icon-minus');
        }
    });
    /* End of bootstrap Add class to accordion **/

    /* Footer Gallery Pretty Photo Widget **/
    $(".gallery-list:first a[rel^='prettyPhoto']").prettyPhoto({
        animation_speed: 'normal',
        theme: 'light_square',
        slideshow: 3000,
        autoplay_slideshow: true
    });

    $(".gallery-page:first a[rel^='prettyPhoto']").prettyPhoto({
        animation_speed: 'normal',
        theme: 'light_square',
        slideshow: 3000,
        autoplay_slideshow: true
    });
    /* End of Footer Gallery Pretty Photo Widget **/

    /* Social Icons **/
    $('.social_active').hoverdir({});
    /* End of Social Icons Animation **/

    /* Conter */
    var austDay = new Date();
    austDay = new Date(2013, 8 - 1, 5, 11, 00)
    $('#countdown162').countdown({
        until: austDay
    });
    $('#year').text(austDay.getFullYear());

});
/* End of Counter */